;************************************ Pulsador_01.asm ***********************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Cada vez que presione el pulsador conectado al pin RA4 incrementa un contador
; visualizado en BCD en la barra de diodos LED. Cuando llegue a 99 (b'1001 1001')
; resetea y comienza de nuevo la cuenta.
;
; ZONA DE DATOS **********************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK  0x0C
	Contador						; El contador a visualizar.
	ENDC

MAXIMO	EQU	.100					; M�ximo valor de la cuenta.
#DEFINE Pulsador	PORTA,4			; Pulsador conectado a RA4.
#DEFINE Display		PORTB			; El display est� conectado al Puerto B.

; ZONA DE C�DIGOS ********************************************************************

	ORG	0							; El programa comienza en la direcci�n 0.
Inicio
  	bsf		STATUS,RP0				; Acceso al Banco 1.
	clrf	Display					; Estas l�neas configuradas como salidas.
	bsf		Pulsador				; L�nea del pulsador configurada como entrada.
	bcf		STATUS,RP0				; Acceso al Banco 0.
	call	InicializaContador		; Inicializa el Contador y lo visualiza.
Principal
	btfsc	Pulsador				; �Pulsador presionado?, �(Pulsador)=0?
	goto	Fin						; No. Vuelve a leerlo.
	call	Retardo_20ms			; Espera que se estabilicen los niveles de tensi�n.
	btfsc	Pulsador				; Comprueba si es un rebote.
	goto	Fin						; Era un rebote y sale fuera.
	call	IncrementaVisualiza		; Incrementa el contador y lo visualiza.
EsperaDejePulsar
	btfss	Pulsador				; �Dej� de pulsar?. �(Pulsador)=1?
	goto	EsperaDejePulsar		; No. Espera que deje de pulsar.
Fin	goto	Principal

; Subrutina "IncrementaVisualiza" ---------------------------------------------------------

IncrementaVisualiza
	incf	Contador,F				; Incrementa el contador y comprueba si ha
	movlw 	MAXIMO					; llegado a su valor m�ximo mediante una
	subwf	Contador,W				; resta. (W)=(Contador)-MAXIMO.
	btfsc	STATUS,C				; �C=0?, �(W) negativo?, �(Contador)<MAXIMO?
InicializaContador
	clrf	Contador				; No, era igual o mayor. Por tanto, resetea.
Visualiza
	movf	Contador,W
	call	BIN_a_BCD				; Lo pasa a BCD y
	movwf	Display					; visualiza.
	return

	INCLUDE <BIN_BCD.INC>			; Subrutina BIN_a_BCD.
	INCLUDE <RETARDOS.INC>			; Subrutinas de retardo.
	END								; Fin del programa.
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
