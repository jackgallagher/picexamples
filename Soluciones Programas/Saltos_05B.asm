;************************************ Saltos_05B.asm *************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Compara el dato del puerto de entrada PORTA con un "NUMERO". Hay tres posibilidades:
;   - Si (PORTA) = NUMERO se encienden todos los diodos LED de salida.
;   - Si (PORTA) > NUMERO se activan los diodos LED pares de salida.
;   - Si (PORTA) < NUMERO se encienden los del nibble alto y se apagan los del bajo.
;
; Otra forma de realizar el ejercicio Saltos_05.asm.

; ZONA DE DATOS **********************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

NUMERO	EQU	d'13'					; Por ejemplo, este n�mero a comparar.

; ZONA DE C�DIGOS ********************************************************************

	ORG 	0						; El programa comienza en la direcci�n 0.
Inicio
	bsf		STATUS,RP0				; Acceso al Banco 1.
	clrf	TRISB					; Las l�neas del Puerto B se configuran como salida.
	movlw	b'00011111'				; Las 5 l�neas del Puerto A se configuran como entrada.
	movwf	TRISA
	bcf		STATUS,RP0				; Acceso al Banco 0.
Principal
	movlw	NUMERO					; Carga el n�mero a comparar.
	subwf	PORTA,W					; (PORTA) - NUMERO --> (W).
	movlw	b'11110000'				; Supone que (PORTA) es menor.
	btfsc	STATUS,C				; �C=0?, �(W) negativo?, �(PORTA) < NUMERO?.
	movlw	b'01010101'				; Supone que (PORTA) es mayor.
	btfsc	STATUS,Z				; �Z=0?, �son distintos?.
	movlw	b'11111111'				; No, por tanto (PORTA) = NUMERO.
	movwf	PORTB					; Resultado se visualiza por el puerto de salida.
	goto 	Principal				; Crea un bucle cerrado e infinito.

	END								; Fin del programa.
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
