;************************************ EEPROM_04.asm **********************************
;
;	====================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS".
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	====================================================================
;
; Programa para grabar una frase en la EEPROM.

; ZONA DE DATOS **********************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK   0x0C
	ENDC

	ORG	0x2100				; Corresponde a la direcci�n 0 de la zona EEPROM
							; de datos.
	DE	"Programa EEPROM_04.asm. Version 2.5. 15-08-2003", 0x00


; ZONA DE C�DIGOS ********************************************************************

	ORG 	0
Inicio
	sleep
	goto	Inicio
	
	END

;	====================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS".
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	====================================================================
