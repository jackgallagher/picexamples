;************************************ Mensaje_4Lineas.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; En el m�dulo LCD de 4 l�neas se visualiza un mensaje que se extiende por las cuatro l�neas.
;
; ZONA DE DATOS **********************************************************************

	LIST	   P=16F84A
	__CONFIG   _CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC
	INCLUDE  <P16F84A.INC>

	CBLOCK  0x0C
	ENDC			

; ZONA DE C�DIGOS *******************************************************************

	ORG	0
Inicio
	call	LCD_Inicializa
	movlw	Mensaje1
	call	LCD_Mensaje
	call	LCD_Linea2
	movlw	Mensaje2
	call	LCD_Mensaje
	call	LCD_Linea3
	movlw	Mensaje3
	call	LCD_Mensaje
	call	LCD_Linea4
	movlw	Mensaje4
	call	LCD_Mensaje
	sleep
	goto	Inicio
	
Mensajes
	addwf	PCL,F
Mensaje1				; Posici�n inicial del mensaje 0.
	DT "    Estudia Ciclo",0x00
Mensaje2				; Posici�n inicial del mensaje 1.
	DT "Formativo DESARROLLO", 0x00
Mensaje3				; Posici�n inicial del mensaje 2.
	DT "    DE PRODUCTOS", 0x00
Mensaje4
	DT "    ELECTRONICOS", 0x00

	INCLUDE  <LCD_MENS.INC>
	INCLUDE  <LCD_4BIT.INC>
	INCLUDE  <RETARDOS.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
