;*********************************** Indirecto_03.asm ***********************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Rellenar una zona de RAM con los datos de una tabla grabada en la memoria de programa.
; Se supone que la tabla nunca exceder� el tama�o de la RAM libre.
; Este programa se comprueba mediante el simulador MPLAB.
;
; ZONA DE DATOS **********************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	RAM_Contador
	ApuntaTabla
	RAM_PrimeraLibre
	ENDC

; ZONA DE C�DIGOS ********************************************************************

	ORG 	0
Inicio
	movlw	TablaFinal-TablaInicio 	; N�mero de posiciones a escribir.
	movwf	RAM_Contador
	movlw	RAM_PrimeraLibre
	movwf	FSR						; Primera direcci�n de memoria RAM a escribir
	clrf	ApuntaTabla				; Primera posici�n de la tabla a leer.
RAM_Escribe
	movf	ApuntaTabla,W
	call	Tabla					; Obtiene el dato de la tabla.
	movwf	INDF					; Escribe en la RAM.
	incf	FSR,F					; Apunta a la siguiente direcci�n de memoria.
	incf	ApuntaTabla,F			; Apunta al siguiente valor de la tabla.
	decfsz	RAM_Contador,F			; Comprueba que ha terminado de escribir todos
	goto	RAM_Escribe				; los valores de la tabla.
Principal
	sleep
	goto	Principal

Tabla
	addwf	PCL,F
TablaInicio
	DT .1,.2,.3,.5,.7,.11,.13		; Por ejemplo, la tabla de primeros n�meros primos.
TablaFinal

	END

;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
