;************************************ Retardo_15.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Las 4 l�neas bajas del Puerto A proporcionan el n�mero inicial de un contador hexadecimal 
; que se visualiza en el display de siete segmentos con una cadencia de 0,5 segundos. 
; El orden ascendente o descendente de la cuenta viene fijado por el valor de la l�nea RA4:
;
; - Si la l�nea RA4 del Puerto A es "0", el contador es descendente. Si por ejemplo, en
;   entrada lee b'1011', la cuenta ser�a: B,A,9,8,7,6,5,4,3,2,1,0,B,A,9,...
; - Si la l�nea RA4 del Puerto A es "1", el contador es ascendente. Si por ejemplo, en
;   entrada lee b'0110', la cuenta ser�a: B,0,1,2,3,4,5,6,7,8,9,A,B,0,1,2,3, ...
;
; ZONA DE DATOS **********************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK 0x0C
	Contador
	ValorInicial
	ENDC

#DEFINE  Display		PORTB
#DEFINE  PinSeleccion	PORTA,4

; ZONA DE C�DIGOS ********************************************************************

	ORG	0
Inicio
	bsf		STATUS,RP0
	clrf	Display					; Configurado como salida.
	bsf		PinSeleccion			; Configurado como entrada.
	bcf		STATUS,RP0
Principal
	movf	PORTA,W					; Lee el puerto de entrada
	andlw	b'00001111'				; qued�ndose con los 4 bits m�s bajos.
	movwf	ValorInicial			; Este es el valor inicial de la cuenta.
	btfss	PinSeleccion			; Lee este pin para saber lo que tiene que hacer.
	goto	Decrementar				; Es 0, entonces a decrementar.
Incrementar							; Es 1, entonces a incrementar.
	clrf	Contador				; Al incrementar, empieza por 0.
Visualiza_e_Incrementa
	call	VisualizaContador
	incf	Contador,F
	movf	Contador,W				; Comprueba si llega al final de la cuenta.
	subwf	ValorInicial,W			; (W)=(ValorInicial)-(Contador).
	btfsc	STATUS,C				; �C=1?, �(W) positivo?, �(ValorInicial)>=(Contador)?
	goto	Visualiza_e_Incrementa	; No. Vuelve a visualizar e incrementar.
	goto	Fin						; S�, resetea y repite el ciclo.
Decrementar
	movf	ValorInicial,W			; Al decrementar, empieza por el n�mero le�do.
	movwf	Contador
Visualiza_y_Decrementa
	call	VisualizaContador
	decfsz	Contador,F
	goto	Visualiza_y_Decrementa
	call	VisualizaContador 		; Visualiza tambi�n el 0.
Fin	clrf	Display					; Se apaga el display y se repite el ciclo.
	call	Retardo_1s
	goto	Principal	

; Subrutina "VisualizaContador" -----------------------------------------------------------

VisualizaContador
	movf	Contador,W
	call	Binario_a_7Segmentos	; Lo pasa a siete segmentos para poder ser
	movwf	Display					; visualizado en el display.
	call	Retardo_500ms			; Durante este tiempo.
	return

; Subrutina "Binario_7Segmentos" --------------------------------------------------------
;
Binario_a_7Segmentos				; Tabla para display de 7 segmentos.
	addwf	PCL,F	
Tabla	retlw	3Fh		; El c�digo 7 segmentos para el "0".
	retlw	06h			; El c�digo 7 segmentos para el "1".
	retlw	5Bh			; El c�digo 7 segmentos para el "2".
	retlw	4Fh			; El c�digo 7 segmentos para el "3".
	retlw	66h			; El c�digo 7 segmentos para el "4".
	retlw	6Dh			; El c�digo 7 segmentos para el "5".
	retlw	7Dh			; El c�digo 7 segmentos para el "6".
	retlw	07h			; El c�digo 7 segmentos para el "7".
	retlw	7Fh			; El c�digo 7 segmentos para el "8".
	retlw	67h			; El c�digo 7 segmentos para el "9".
	retlw	77h			; El c�digo 7 segmentos para el "A".
	retlw	7Ch			; El c�digo 7 segmentos para el "B".
	retlw	39h			; El c�digo 7 segmentos para el "C".
	retlw	5Eh			; El c�digo 7 segmentos para el "D".
	retlw	79h			; El c�digo 7 segmentos para el "E".
	retlw	71h			; El c�digo 7 segmentos para el "F".

	INCLUDE  <RETARDOS.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
