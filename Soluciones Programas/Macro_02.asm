;*********************************** Macro_02.asm *************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Es el mismo ejercicio que Macro_01.asm, pero resueltos con macros.
;
; ZONA DE DATOS **********************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	Contador1
	Contador2
	ENDC

MINIMO_1 EQU	.3
MAXIMO_1	EQU	.16
MINIMO_2	EQU	.7
MAXIMO_2	EQU	.21

#DEFINE  Pulsador 	PORTA,4			; L�nea donde se conecta el pulsador.

; ZONA DE C�DIGOS *******************************************************************

	INCLUDE  <MACROS.INC>			; La definici�n de las macros se deben realizar
									; antes de que esta sean invocadas.
	ORG 	0
Inicio
	call	LCD_Inicializa
	bsf		STATUS,RP0				; Acceso al Banco 1.
	bsf		Pulsador				; L�nea del pulsador configurada como entrada.
	bcf		STATUS,RP0				; Acceso al Banco 0.
	movlw	MINIMO_1				; Inicializa los contadores
	movwf	Contador1
	movlw	MINIMO_2
	movwf	Contador2
	call	VisualizaContadores
Principal
	btfss	Pulsador				; Lee el pulsador.
	call	IncrementaVisualiza		; Salta a incrementar y visualizar el contador.
	goto	Principal
;
; Subrutina "IncrementaVisualiza" -------------------------------------------------------
;
; Subrutina que incrementa el contador y lo visualiza.
;
IncrementaVisualiza
	Incrementa	Contador1,MINIMO_1,MAXIMO_1,IncrementaContador2
IncrementaContador2
	Incrementa	Contador2,MINIMO_2,MAXIMO_2,VisualizaContadores
VisualizaContadores
	call	LCD_Linea1
	VisualizaBCD	Contador1
	call	LCD_Linea2
	VisualizaBCD	Contador2
	call	Retardo_200ms
	return
;
	INCLUDE  <RETARDOS.INC>
	INCLUDE  <BIN_BCD.INC>
	INCLUDE  <LCD_4BIT.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
