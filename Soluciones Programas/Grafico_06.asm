;************************************** Grafico_06.asm **********************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADORES PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Muestra la animaci�n de un comecocos persiguiendo a un coco por la l�nea 1
; del LCD y vuelve persigui�ndole por la l�nea 2 del LCD
;
; ZONA DE DATOS *************************************************************************

	LIST	 P=16F84A
	INCLUDE  <P16F84A.INC>
	__CONFIG   _CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK 0x0C
	Contador
	Posicion_x
	ENDC
	
LONGITUD_LINEA EQU .16

; ZONA DE C�DIGOS ********************************************************************

	ORG	0
Inicio
	call	LCD_Inicializa			; Prepara la pantalla.
	call	CGRAM_EscribeDatos		; Escribe en la CGRAM los s�mbolos de los comecocos.
Principal
	call	LCD_Borra
	call	Retardo_1s
	movlw	0x03
	call	LCD_Caracter
	call	Retardo_500ms
	movlw	LONGITUD_LINEA+1
	movwf	Contador
	clrf	Posicion_x
DesplazaDerecha
	call	LCD_Borra
	incf	Posicion_x,W
	call	LCD_PosicionLinea1
	movlw	0x03
	call	LCD_Caracter
	call	ComecocosDerecha
	incf	Posicion_x,F
	decfsz	Contador,f
	goto	DesplazaDerecha
;
	movlw	LONGITUD_LINEA+1
	movwf	Contador
	movwf	Posicion_x
DesplazaIzquierda
	call	LCD_Borra
	decf	Posicion_x,W
	call	LCD_PosicionLinea2
	movlw	0x03
	call	LCD_Caracter
	call	ComecocosIzquierda
	decf	Posicion_x,F
	decfsz	Contador,f
	goto	DesplazaIzquierda
	call	LCD_Borra
	call	ComecocosIzquierda
	goto	Principal
	
; Subrutina ComecocosDerecha -----------------------------------------------------------
;
; Visualiza el comecocos animado hacia la derecha en la l�nea 1.
;
ComecocosDerecha
	movf	Posicion_x,W
	call	LCD_PosicionLinea1
	movlw	0x00
	call	LCD_Caracter
	call	Retardo_200ms
	movf	Posicion_x,W
	call	LCD_PosicionLinea1
	movlw	0x01
	call	LCD_Caracter
	call	Retardo_200ms
	movf	Posicion_x,W
	call	LCD_PosicionLinea1
	movlw	0x02
	call	LCD_Caracter
	call	Retardo_200ms
	return

; Subrutina ComecocosIzquierda -----------------------------------------------------------
;
; Visualiza el comecocos animado hacia la izquierda en la l�nea 2.
;
ComecocosIzquierda
	movf	Posicion_x,W
	call	LCD_PosicionLinea2
	movlw	0x04
	call	LCD_Caracter
	call	Retardo_200ms
	movf	Posicion_x,W
	call	LCD_PosicionLinea2
	movlw	0x05
	call	LCD_Caracter
	call	Retardo_200ms
	movf	Posicion_x,W
	call	LCD_PosicionLinea2
	movlw	0x06
	call	LCD_Caracter
	call	Retardo_200ms
	return

; Subrutina LCD_Escribe CGRAM -----------------------------------------------------------
;
; Escribe en la CGRAM los s�mbolos de los "comecocos".
;
CGRAM_NUM_IMAGENES EQU 8
;
CGRAM_EscribeDatos
	movlw 	b'01000000'				; Comando indicando que va a escribir a partir de
	call 	LCD_EnviaComando		; la direcci�n de la CGRAM.
	clrf	Contador
CGRAM_EscribeOtro
	movf	Contador,W
	call	CGRAM_Escribe
	call	LCD_Caracter
	incf	Contador,F
	movlw	CGRAM_NUM_IMAGENES * 8	; Cada imagen ocupa 8 posiciones de CGRAM.
	subwf	Contador,W
	btfss	STATUS,C
	goto	CGRAM_EscribeOtro	
	call	LCD_Borra
	return	

CGRAM_Escribe
	addwf	PCL,F

; S�mbolo comecoco, byte 0 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010111'		;  |   x xxx|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00000000'		;  |        |
							;   --------	

; S�mbolo comecoco, byte 1 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00010010'		;  |   x  x |
	retlw	b'00010100'		;  |   x x  |
	retlw	b'00010010'		;  |   x  x |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00000000'		;  |        |
							;   --------	

; S�mbolo comecoco, byte 2 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00010100'		;  |   x x  |
	retlw	b'00011000'		;  |   xx   |
	retlw	b'00010100'		;  |   x x  |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00000000'		;  |        |
							;   --------	

; S�mbolo comecoco, byte 3 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00000000'		;  |        |
							;   --------	

; S�mbolo comecoco, byte 4 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00011101'		;  |   xxx x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00000000'		;  |        |
							;   --------	

; S�mbolo comecoco, byte 5 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00001001'		;  |    x  x|
	retlw	b'00000101'		;  |     x x|
	retlw	b'00001001'		;  |    x  x|
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00000000'		;  |        |
							;   --------	

; S�mbolo comecoco, byte 6 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00000101'		;  |     x x|
	retlw	b'00000011'		;  |      xx|
	retlw	b'00000101'		;  |     x x|
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00000000'		;  |        |
							;   --------
							
	INCLUDE  <RETARDOS.INC>
	INCLUDE  <LCD_4BIT.INC>

	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADORES PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================

