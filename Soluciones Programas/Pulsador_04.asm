;************************************ Pulsador_04.asm ***********************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Cada vez que presione el pulsador conectado al pin RA4 incrementa un contador visualizado
; en la barra de LED. La cuenta ser� en 8 bits desde 0 hasta 255.
;
; ZONA DE DATOS **********************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK  0x0C
	Contador						; El contador a visualizar.
	ENDC

#DEFINE Pulsador	PORTA,4			; Pulsador conectado a RA4.
#DEFINE Salida		PORTB			; Puerto donde se conecta los diodos LED.

; ZONA DE C�DIGOS ********************************************************************

	ORG	0							; El programa comienza en la direcci�n 0.
Inicio
  	bsf		STATUS,RP0				; Acceso al Banco 1.
	clrf	Salida					; Estas l�neas configuradas como salidas.
	bsf		Pulsador				; L�nea del pulsador configurada como entrada.
	bcf		STATUS,RP0				; Acceso al Banco 0.
	clrf	Contador				; Inicializa contador.
	clrf	Salida					; En principio salida apagada.
Principal
	btfsc	Pulsador				; �Pulsador presionado?, �(Pulsador)=0?
	goto	Fin						; No. Vuelve a leerlo.
	call	Retardo_20ms			; Espera que se estabilicen los niveles de tensi�n.
	btfsc	Pulsador				; Comprueba si es un rebote.
	goto	Fin						; Era un rebote y sale fuera.
	incf	Contador,F				; Incrementa el contador.
	movf	Contador,W
	movwf	Salida					; Visualiza por la salida.
EsperaDejePulsar
	btfss	Pulsador				; �Dej� de pulsar?. �(Pulsador)=1?
	goto	EsperaDejePulsar		; No. Espera que deje de pulsar.
Fin	goto	Principal

	INCLUDE <RETARDOS.INC>
	END

;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
