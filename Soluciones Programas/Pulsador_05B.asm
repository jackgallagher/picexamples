;************************************ Pulsador_05B.asm ***********************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Cada vez que presiona el pulsador conectado a la l�nea RA4 conmutar� el estado de
; un LED conectado a la l�nea RB1. La complementaci�n se realiza con ayuda de una 
; funci�n XOR.
;
; ZONA DE DATOS **********************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC
	
	CBLOCK 0x0C
	ENDC

#DEFINE Pulsador	PORTA,4		; Pulsador conectado a RA4.

; ZONA DE C�DIGOS ********************************************************************

	ORG	0						; El programa comienza en la direcci�n 0.
Inicio
 	bsf		STATUS,RP0			; Acceso al Banco 1.
	bsf		Pulsador			; La l�nea RA4 como entrada.
	clrf	PORTB				; Se configura como salida.
	bcf		STATUS,RP0			; Acceso al Banco 0.
	clrf	PORTB				; En principio diodo LED apagado.
Principal
	btfsc	Pulsador			; �Pulsador presionado?, �(Pulsador)=0?
	goto	Fin					; No. Vuelve a leerlo.
	call	Retardo_20ms		; Espera que se estabilicen los niveles de tensi�n.
	btfsc	Pulsador			; Comprueba si es un rebote.
	goto	Fin					; Era un rebote y sale fuera.
	movlw	b'00000010'			; Se hace una XOR para complementar.
	xorwf	PORTB,F
EsperaDejePulsar
	btfss	Pulsador			; �Dej� de pulsar?. �(Pulsador)=1?
	goto	EsperaDejePulsar	; No. Espera que deje de pulsar.
Fin
	goto	Principal

	INCLUDE <RETARDOS.INC>
	END

;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
