;********************************* Aritmetica_12.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Programa donde se comprueba la forma de convertir un n�mero binario de 16 bits a un
; n�mero decimal de 5 digitos. Un ejemplo se visualizar� en el LCD pero sin representar
; los ceros no significativos de la izquierda, utilizando para ello la subrutina 
; Arit_VisualizaBCD de la librer�a Aritmetica.inc.
;
; As� por ejemplo:
; - En la primera l�nea visualiza: "Hex. = 25E9".
; - En la segunda l�nea visualiza: "Dec. = 9705"
;
; ZONA DE DATOS *************************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC


	CBLOCK	0x0C
	ENDC

BINARIO		EQU	0x25E9				; El n�mero a convertir.

; ZONA DE C�DIGOS ***********************************************************************

	ORG 	0
Inicio
	call	LCD_Inicializa
	movlw	MensajeHex
	call	LCD_Mensaje
	movlw	HIGH BINARIO			; Carga todos los registros y visualiza en hexadecimal.
	movwf	Arit_Binario_H
	call	LCD_ByteCompleto
	movlw	LOW BINARIO
	movwf	Arit_Binario_L
	call	LCD_ByteCompleto
	call	Arit_Bin_BCD_16Bit		; Pasa de binario natural en 16 bits a BCD de 5 digitos.
	call	LCD_Linea2				; Pasa a la segunda l�nea del LCD.
	movlw	MensajeBCD				; Visualiza el resultado en decimal.
	call	LCD_Mensaje
	call	Arit_VisualizaBCD		; Visualiza el resultado sin representar los ceros no
	sleep							; significativos.

Mensajes
	addwf	PCL,F
MensajeBCD
	DT "Dec. = ", 0x00
MensajeHex
	DT "Hex. = ", 0x00

	INCLUDE	<ARITMETICA.INC>		; Subrutinas Arit_Bin_BCD_16Bit y Arit_VisualizaBCD.
	INCLUDE	<RETARDOS.INC>
	INCLUDE	<LCD_4BIT.INC>
	INCLUDE <LCD_MENS.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
