;************************************ Retardo_14.asm ***********************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Por los diodos LED conectados al puerto de salida se visualizar� un contador binario
; que se incrementa cada 500 ms. La cuenta ser� en 8 bits desde 0 hasta 255.
;
; ZONA DE DATOS **********************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK  0x0C
	Contador						; El contador a visualizar.
	ENDC

; ZONA DE C�DIGOS ********************************************************************

	ORG	0							; El programa comienza en la direcci�n 0.
Inicio
  	bsf		STATUS,RP0				; Acceso al Banco 1.
	clrf	PORTB					; Estas l�neas configuradas como salidas.
	bcf		STATUS,RP0				; Acceso al Banco 0.
	clrf	Contador				; Inicializa contador.
Principal
	incf	Contador,F				; Incrementa el contador.
	movf	Contador,W
	movwf	PORTB					; Visualiza por la salida.
	call 	Retardo_500ms			; Retardo entre cada valor del contador.
	goto	Principal

	INCLUDE <RETARDOS.INC>
	END

;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
