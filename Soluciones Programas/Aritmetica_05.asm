;********************************* Aritmetica_05.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Programa donde se comprueba la forma de restar dos n�meros de 16 bit, mediante un ejemplo
; que se visualiza en un LCD..
;
; Utiliza la subrutina Arit_Resta_16Bit de la libreria Aritmetica.inc que realiza la resta
; en 16 bits: (Operando 2) - (Operando 1) y el resultado lo guarda en (Operando 2) en 16 bits.
; Es decir: (Arit_Operando_2H, Arit_Operando_2L) - (Arit_Operando_1H, Arit_Operando_1L) --> 
; (Arit_Operando_2H, Arit_Operando_2L)
;
; ZONA DE DATOS *************************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	ENDC

MINUENDO	EQU	0x7DF6				; Por ejemplo realizar� la resta de estos n�meros de
SUSTRAENDO	EQU	0x58AB				; dos bytes (16 bits): MINUENDO - SUSTRAENDO

; ZONA DE C�DIGOS ***********************************************************************

	ORG 	0
Inicio
	call	LCD_Inicializa
	movlw	HIGH MINUENDO			; Extrae los bytes y los lleva a los registros
	movwf	Arit_Operando_2H		; utilizados por la subrutina Arit_Resta_16Bit.
	call	LCD_ByteCompleto
	movlw	LOW MINUENDO
	movwf	Arit_Operando_2L
	call	LCD_ByteCompleto
	movlw	'-'						; Visualiza signo de resta.
	call	LCD_Caracter
	movlw	HIGH SUSTRAENDO
	movwf	Arit_Operando_1H
	call	LCD_ByteCompleto
	movlw	LOW SUSTRAENDO
	movwf	Arit_Operando_1L
	call	LCD_ByteCompleto
	movlw	'='						; Visualiza signo de igualdad.
	call	LCD_Caracter
	call	Arit_Resta_16Bit		; Realiza la resta.
	movf	Arit_Operando_2H,W		; y ahora visualiza el resultado.
	call	LCD_ByteCompleto
	movf	Arit_Operando_2L,W
	call	LCD_ByteCompleto
	sleep							; Modo reposo.

	INCLUDE	<ARITMETICA.INC>		; Subrutina Arit_Resta_16Bit
	INCLUDE	<RETARDOS.INC>
	INCLUDE	<LCD_4BIT.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================

