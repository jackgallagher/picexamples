;************************************** Grafico_04.asm **********************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADORES PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Muestra la simbolo de un comecocos abriendo y cerrando la boca detr�s de un coco, ambos
; inm�viles en el mismo sitio del LCD.
;
; ZONA DE DATOS *************************************************************************

	LIST	 P=16F84A
	INCLUDE  <P16F84A.INC>
	__CONFIG   _CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK 0x0C
	Contador
	ENDC

; ZONA DE C�DIGOS ********************************************************************

	ORG	0
Inicio
	call	LCD_Inicializa			; Prepara la pantalla.
	call	CGRAM_EscribeDatos		; Escribe en la CGRAM los simbolos de la animaci�n
	movlw	0x02					; En la posici�n 2 de la l�nea 2 dibuja el coco.
	call	LCD_PosicionLinea1
	movlw	0x03					; El s�mbolo del coco est� en esta posici�n de la
	call	LCD_Caracter			; CGRAM.
Principal
	movlw	0x01					; En la posici�n 1 de la l�nea 1 dibuja el comecocos.
	call	LCD_PosicionLinea1		; en las diferentes fases de la animaci�n.
	movlw	0x00					; En el byte 0 de la CGRAM est� el comecocos con la
	call	LCD_Caracter			; boca cerrada.
	call	Retardo_200ms
	movlw	0x01
	call	LCD_PosicionLinea1
	movlw	0x01					; En el byte 1 de la CGRAM est� el comecocos con la
	call	LCD_Caracter			; boca entreabierta.
	call	Retardo_200ms
	movlw	0x01
	call	LCD_PosicionLinea1
	movlw	0x02					; En el byte 2 de la CGRAM est� el comecocos con la
	call	LCD_Caracter			; boca abierta.
	call	Retardo_200ms
	goto	Principal

; Subrutina LCD_Escribe CGRAM -----------------------------------------------------------
;
; Escribe en la CGRAM los s�mbolos de los comecocos.
;
CGRAM_NUM_IMAGENES EQU 4
;
CGRAM_EscribeDatos
	movlw 	b'01000000'				; Comando indicando que va a escribir a partir de
	call 	LCD_EnviaComando		; la direcci�n de la CGRAM.
	clrf	Contador
CGRAM_EscribeOtro
	movf	Contador,W
	call	CGRAM_Escribe
	call	LCD_Caracter
	incf	Contador,F
	movlw	CGRAM_NUM_IMAGENES * 8	; Cada imagen ocupa 8 posiciones de CGRAM.
	subwf	Contador,W
	btfss	STATUS,C
	goto	CGRAM_EscribeOtro	
	call	LCD_Borra
	return	

CGRAM_Escribe
	addwf	PCL,F

; S�mbolo comecoco boca cerrada, byte 0 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010111'		;  |   x xxx|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00000000'		;  |        |
							;   --------	

; S�mbolo comecoco boca entreabierta, byte 1 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00010010'		;  |   x  x |
	retlw	b'00010100'		;  |   x x  |
	retlw	b'00010010'		;  |   x  x |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00000000'		;  |        |
							;   --------	

; S�mbolo comecoco boca abierta, byte 2 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00010100'		;  |   x x  |
	retlw	b'00011000'		;  |   xx   |
	retlw	b'00010100'		;  |   x x  |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00000000'		;  |        |
							;   --------	

; S�mbolo del coco, byte 3 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00000000'		;  |        |
							;   --------	

	INCLUDE  <RETARDOS.INC>
	INCLUDE  <LCD_4BIT.INC>

	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADORES PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================

