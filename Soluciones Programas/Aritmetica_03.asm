;********************************* Aritmetica_03.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Programa donde se comprueba la forma de negar un n�mero de 16 bits.
; En la primera l�nea del LCD aparece en binario el valor de una constante de
; 16 bits, en la segunda ese mismo n�mero negado.
;
; Utiliza la subrutina Arit_Negar que recibe el dato de 16 bits a trav�s de los registros
; (Arit_Operando_1L, Arit_Operando_1H), y el resultado lo entrega por los mismos registros.
;
; ZONA DE DATOS *************************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	ENDC

NUMERO	EQU	0x7AD6					; Por ejemplo este n�mero de dos bytes.

; ZONA DE C�DIGOS ***********************************************************************

	ORG 	0
Inicio
	call	LCD_Inicializa
	movlw	HIGH NUMERO				; El operador HIGH extrae el byte alto y lo lleva
	movwf	Arit_Operando_1H		; al registro correspondiente.
	call	Arit_VisualizaBinario	; Visualiza en binario.
	movlw	LOW NUMERO				; El operador LOW extrae el byte bajo y lo lleva
	movwf	Arit_Operando_1L		; al registro correspondiente.
	call	Arit_VisualizaBinario	; Visualiza en binario.
	call	LCD_Linea2
	call	Arit_Negar				; Niega el n�mero.
	movf	Arit_Operando_1H,W		; Visualiza en binario el byte alto del n�mero
	call	Arit_VisualizaBinario	; negado.
	movf	Arit_Operando_1L,W		; Visualiza en binario el byte bajo del n�mero
	call	Arit_VisualizaBinario	; negado.
	sleep							; Pasa a modo de bajo consumo.

	INCLUDE	<ARITMETICA.INC>		; Subrutinas Arit_VisualizaBinario y Arit_Negar.
	INCLUDE	<RETARDOS.INC>
	INCLUDE	<LCD_4BIT.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
