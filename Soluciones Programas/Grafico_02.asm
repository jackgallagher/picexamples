;************************************** Grafico_02.asm **********************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Muestra la animaci�n de la carga de una bateria desde 0% a 100%
;
; ZONA DE DATOS *************************************************************************

	LIST	 P=16F84A
	INCLUDE  <P16F84A.INC>
	__CONFIG   _CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK 0x0C
	Contador
	ENDC

; ZONA DE C�DIGOS ********************************************************************

	ORG	0
Inicio
	call	LCD_Inicializa			; Prepara la pantalla LCD.
	call	CGRAM_EscribeDatos
	movlw	MensajeCargando
	call	LCD_Mensaje
Principal
	clrf	Contador
VisualizaImagen
	call	LCD_Linea1				; Se sit�a al principio de la l�nea 1.
	movf	Contador,W
	call	LCD_Caracter
	call	Retardo_200ms
	incf	Contador,F
	movlw	.6						; La bateria animada est� compuesta por 6 im�genes.
	subwf	Contador,W
	btfss	STATUS,C
	goto	VisualizaImagen
	goto	Principal
	
; Subrutina LCD_Escribe CGRAM -----------------------------------------------------------
;
; Escribe en la CGRAM los s�mbolos de la bateria para la animaci�n.
;
CGRAM_NUM_IMAGENES EQU 6
;
CGRAM_EscribeDatos
	movlw 	b'01000000'				; Comando indicando que va a escribir a partir de
	call 	LCD_EnviaComando		; la direcci�n de la CGRAM.
	clrf	Contador
CGRAM_EscribeOtro
	movf	Contador,W
	call	CGRAM_Escribe
	call	LCD_Caracter
	incf	Contador,F
	movlw	CGRAM_NUM_IMAGENES * 8	; Cada imagen ocupa 8 posiciones de CGRAM.
	subwf	Contador,W
	btfss	STATUS,C
	goto	CGRAM_EscribeOtro	
	call	LCD_Borra
	return	

CGRAM_Escribe
	addwf	PCL,F
	
; Bateria al 0%, byte 0 de la CGRAM.
							;   ________
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00001010'		;  |    x x |
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00011111'		;  |   xxxxx|
							;   --------
; Bateria al 20%, byte 1 de la CGRAM.
							;   ________
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00001010'		;  |    x x |
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
							;   --------
; Bateria al 40%, byte 2 de la CGRAM.
							;   ________
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00001010'		;  |    x x |
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
							;   --------
; Bateria al 60%, byte 3 de la CGRAM.
							;   ________
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00001010'		;  |    x x |
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
							;   --------
; Bateria al 80%, byte 4 de la CGRAM.
							;   ________
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00001010'		;  |    x x |
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
							;   --------
; Bateria al 100%, byte 5 de la CGRAM.
							;   ________
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00001010'		;  |    x x |
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
							;   --------

; "Mensajes" ----------------------------------------------------------------------------
;
Mensajes
	addwf	PCL,F
MensajeCargando
	DT "   Cargando", 0x00

	INCLUDE  <RETARDOS.INC>
	INCLUDE  <LCD_4BIT.INC>
	INCLUDE  <LCD_MENS.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================

