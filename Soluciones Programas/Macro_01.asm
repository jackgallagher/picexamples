;*********************************** Macro_01.asm *************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Se conectar� un pulsador al pin RA4 y mientras se mantenga pulsado se incrementar�n dos
; contadores distintos que se visualizar�n en la pantalla del modulo LCD:
;   - El Contador1 se visualiza en la l�nea 1 y cuenta de 3 a 16.
;   - El Contador2 se visualiza en la l�nea 2 y cuenta de 7 a 21.
;
; ZONA DE DATOS **********************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	Contador1
	Contador2
	ENDC

MINIMO_1 	EQU	.3
MAXIMO_1	EQU	.16
MINIMO_2	EQU	.7
MAXIMO_2	EQU	.21

#DEFINE  Pulsador 	PORTA,4			; L�nea donde se conecta el pulsador.

; ZONA DE C�DIGOS ********************************************************************

	ORG 	0
Inicio
	call	LCD_Inicializa
	bsf		STATUS,RP0				; Acceso al Banco 1.
	bsf		Pulsador				; L�nea del pulsador configurada como entrada.
	bcf		STATUS,RP0				; Acceso al Banco 0.
	movlw	MINIMO_1					; Inicializa los contadores
	movwf	Contador1
	movlw	MINIMO_2
	movwf	Contador2
	call	VisualizaContadores
Principal
	btfss	Pulsador				; Lee el pulsador.
	call	IncrementaVisualiza		; Salta a incrementar y visualizar el contador.
	goto	Principal
;
; Subrutina "IncrementaVisualiza" -------------------------------------------------------
;
; Subrutina que incrementa el contador y lo visualiza.
;
IncrementaVisualiza
	incf	Contador1,F				; Incrementa el contador 1.
	movf	Contador1,W				; �Ha llegado a su valor m�ximo?
	sublw	MAXIMO_1					; (W) = MAXIMO_1 - (Contador1).
	btfsc	STATUS,C				; �C=0?, �(W) negativo?, �MAXIMO_1<(Contador1)?
	goto	IncrementaContador2		; No, ha resultado MAXIMO_1>=(Contador1)
	movlw	MINIMO_1					; S�, ha resultado MAXIMO_1<(Contador1), entonces
	movwf	Contador1				; inicializa el registro.
IncrementaContador2
	incf	Contador2,F				; Incrementa el contador 2.
	movf	Contador2,W				; �Ha llegado a su valor m�ximo?
	sublw	MAXIMO_2					; (W) = MAXIMO_2 - (Contador2).
	btfsc	STATUS,C				; �C=0?, �(W) negativo?, �MAXIMO_2<(Contador2)?
	goto	VisualizaContadores		; No, ha resultado MAXIMO_2>=(Contador2)
	movlw	MINIMO_2					; S�, ha resultado MAXIMO_2<(Contador2), entonces
	movwf	Contador2				; inicializa el registro.
VisualizaContadores
	call	LCD_Linea1
	movf	Contador1,W
	call	BIN_a_BCD
	call	LCD_Byte
	call	LCD_Linea2
	movf	Contador2,W
	call	BIN_a_BCD
	call	LCD_Byte
	call	Retardo_200ms			; Se incrementa cada 200 ms.
	return
;
	INCLUDE  <RETARDOS.INC>
	INCLUDE  <BIN_BCD.INC>
	INCLUDE  <LCD_4BIT.INC>
	END

;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
