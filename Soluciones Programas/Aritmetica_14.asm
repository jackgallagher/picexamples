;********************************* Aritmetica_14.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Programa ejemplo de utilizaci�n de subrutinas aritm�ticas. En el LCD se visualizar� una
; divisi�n en decimal. Por ejemplo:
; - En la primera l�nea del LCD: "35089�108=324".
; - En la segunda l�nea del LCD: "Resto = 97" 
;
; Utiliza la subrutina Arit_Divide_16Bit de la libreria Aritmetica.inc que realiza la
; divisi�n como se indica mediante los siguientes registros:
; (Arit_Dividendo_H, Arit_Dividendo_L) / (Arit_Divisor) = (Arit_Cociente_H, Arit_Cociente_L)
; y el resto en (Arit_Resto).
;
; La divisi�n se har� sin signo y el divisor ser� de 7 bits, es decir su valor m�ximo ser�:
; b'01111111'=0x7F=d'127'. El m�ximo valor posible del dividendo ser� 65535.
;
; ZONA DE DATOS *************************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	GuardaResto
	ENDC

DIVIDENDO	EQU	.35089				; Por ejemplo utilizar� estos n�meros.
DIVISOR		EQU	.108

; ZONA DE C�DIGOS ***********************************************************************

	ORG 	0
Inicio
	call	LCD_Inicializa
	movlw	DIVISOR					; Carga el divisor y comprueba que no es igual a cero.
	movwf	Arit_Divisor
	movf	Arit_Divisor,F
	btfss	STATUS,Z
	goto	VisualizaDividendo
	movlw	MensajeError			; Si el divisor es cero, visualiza un mensaje de error.
	call	LCD_Mensaje
	goto	Fin
VisualizaDividendo
	movlw	HIGH DIVIDENDO			; Visualiza dividendo en decimal.
	movwf	Arit_Binario_H
	movlw	LOW DIVIDENDO
	movwf	Arit_Binario_L
	call	Arit_Bin_BCD_16Bit		; Pasa de binario natural en 16 bits a BCD de 5 digitos.
	call	Arit_VisualizaBCD		; Lo visualiza.
	movlw	0xFD					; Posici�n del signo de la divisi�n para LCD,
	call	LCD_Caracter			; (ver tabla de la figura 13-4 del libro).
	clrf	Arit_Binario_H			; Visualiza el divisor en decimal.
	movlw	LOW DIVISOR
	movwf	Arit_Binario_L
	call	Arit_Bin_BCD_16Bit		; Pasa de binario natural en 16 bits a BCD de 5 digitos.
	call	Arit_VisualizaBCD		; Lo visualiza
	movlw	'='						; Visualiza signo de igualdad.
	call	LCD_Caracter
;
	movlw	HIGH DIVIDENDO			; Carga todos los registros para realizar la divisi�n.
	movwf	Arit_Dividendo_H
	movlw	LOW DIVIDENDO
	movwf	Arit_Dividendo_L
	movlw	DIVISOR
	movwf	Arit_Divisor
	call	Arit_Divide_16Bit		; Realiza la divisi�n.
;
	movwf	GuardaResto				; Salvaguarda el valor del resto.
	movf	Arit_Cociente_H,W		; Y ahora visualiza el cociente.
	movwf	Arit_Binario_H
	movf	Arit_Cociente_L,W
	movwf	Arit_Binario_L
	call	Arit_Bin_BCD_16Bit		; Pasa de binario natural en 16 bits a BCD de 5 digitos.
	call	Arit_VisualizaBCD		; Visualiza el cociente.
;
	call	LCD_Linea2				; Ahora visualiza el resto en la segunda l�nea.
	movlw	MensajeResto
	call	LCD_Mensaje
	clrf	Arit_Binario_H
	movf	GuardaResto,W			; Recupera el valor del resto.
	movwf	Arit_Binario_L
	call	Arit_Bin_BCD_16Bit		; Pasa de binario natural en 16 bits a BCD de 5 digitos.
	call	Arit_VisualizaBCD		; Visualiza el resto.
Fin	sleep							; Pasa a modo de bajo consumo.

Mensajes
	addwf	PCL,F
MensajeError
	DT "Divide por CERO!",0x00
MensajeResto
	DT "Resto = ", 0x00

	INCLUDE	<ARITMETICA.INC>
	INCLUDE	<RETARDOS.INC>
	INCLUDE	<LCD_4BIT.INC>
	INCLUDE <LCD_MENS.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
