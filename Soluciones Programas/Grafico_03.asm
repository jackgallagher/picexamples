;************************************** Grafico_03.asm **********************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADORES PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Muestra en la primera l�nea de la pantalla del LCD los siguientes s�mbolos: 
; tel�fono colgado, tel�fono descolgado, y un comecocos en diferentes fases de apertura
; de la boca.
;
; ZONA DE DATOS *************************************************************************

	LIST	 P=16F84
	INCLUDE  <P16F84A.INC>
	__CONFIG   _CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK 0x0C
	Contador
	ENDC
	
; ZONA DE C�DIGOS ***********************************************************************

	ORG	0x0
Inicio
	call	LCD_Inicializa
	call	CGRAM_EscribeDatos	;Escribe en la CGRAM los simbolos de la bateria para la animaci�n
Principal
	clrf	Contador			; Primero apunta a la imagen situada en el byte 0 de CGRAM.
VisualizaImagen
	movf	Contador,W			; Visualiza la imagen apuntada por Contador de la CGRAM.
	call	LCD_Caracter
	incf	Contador,F			; Apunta a la siguiente imagen.
	movf	Contador,W
	sublw	CGRAM_NUM_IMAGENES	; (W) = CGRAM_NUM_IMAGENES - (Contador)
	btfsc	STATUS,C			; �Ha terminado de imprimir todas las imagenes?
	goto	VisualizaImagen
	sleep						; Pasa a bajo consumo.
	goto	Principal

; Subrutina LCD_Escribe CGRAM -----------------------------------------------------------
;
; Escribe en la CGRAM los s�mbolos.
;
CGRAM_NUM_IMAGENES EQU 6
;
CGRAM_EscribeDatos
	movlw 	b'01000000'				; Comando indicando que va a escribir a partir de
	call 	LCD_EnviaComando		; la direcci�n de la CGRAM.
	clrf	Contador
CGRAM_EscribeOtro
	movf	Contador,W
	call	CGRAM_Escribe
	call	LCD_Caracter
	incf	Contador,F
	movlw	CGRAM_NUM_IMAGENES * 8	; Cada imagen ocupa 8 posiciones de CGRAM.
	subwf	Contador,W
	btfss	STATUS,C
	goto	CGRAM_EscribeOtro	
	call	LCD_Borra
	return	

CGRAM_Escribe
	addwf	PCL,F

; Simbolo tel�fono colgado, byte 0 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00010101'		;  |   x x x|
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00011111'		;  |   xxxxx|
							;   --------	
; Simbolo tel�fono descolgado, byte 1 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00000000'		;  |        |
	retlw	b'00000100'		;  |     x  |
	retlw	b'00000100'		;  |     x  |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00011111'		;  |   xxxxx|
							;   --------	
; Simbolo comecoco_1, byte 2 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010111'		;  |   x xxx|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00001110'		;  |    xxx |
							;   --------	
; Simbolo comecoco_2,  byte 3 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00010010'		;  |   x  x |
	retlw	b'00010100'		;  |   x x  |
	retlw	b'00010010'		;  |   x  x |
	retlw	b'00001110'		;  |    xxx |
							;   --------	
; Simbolo comecoco_3,  byte 4 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00010100'		;  |   x x  |
	retlw	b'00011000'		;  |   xx   |
	retlw	b'00010100'		;  |   x x  |
	retlw	b'00001110'		;  |    xxx |
							;   --------	
; Simbolo comecoco_4,  byte 5 de la CGRAM.
							;   ________
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00000000'		;  |        |
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00011111'		;  |   xxxxx|
	retlw	b'00001110'		;  |    xxx |
							;   --------

	INCLUDE  <RETARDOS.INC>
	INCLUDE  <LCD_4BIT.INC>

	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADORES PIC16F87x. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================

