;********************************* Aritmetica_07.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. López.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Programa donde se comprueba la forma de multiplicar dos números de 16 bits que se
; representa en el visualizador LCD. La multiplicación se realiza sin signo.
;
; Utiliza la subrutina Arit_Multiplica_16Bit de la libreria Aritmetica.inc que realiza la
; multiplicación como indica: 
; (Multiplicando en 16 bits) * (Multiplicador en 16 bits) = (Resultado en 32 bits).
; (Arit_Multiplicando_H, Arit_Multiplicando_L) * 
; (Arit_Multiplicador_H, Arit_Multiplicador_L) --> 
; (Arit_Producto_2H, Arit_Producto_2L, Arit_Producto_1H, Arit_Producto_1L).
;
; ZONA DE DATOS *************************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	ENDC

MULTIPLICANDO	EQU	0xB8DA			; Por ejemplo utilizará estos números.
MULTIPLICADOR	EQU	0xFE9D

; ZONA DE CÓDIGOS ***********************************************************************

	ORG 	0
Inicio
	call	LCD_Inicializa
	movlw	HIGH MULTIPLICANDO		; Extrae los bytes, los lleva a los registros que 
	movwf	Arit_Multiplicando_H	; utiliza la subrutina Arit_Multiplica_16Bit.
	call	LCD_ByteCompleto		; Visualiza en el LCD.
	movlw	LOW MULTIPLICANDO
	movwf	Arit_Multiplicando_L
	call	LCD_ByteCompleto
	movlw	'*'						; Simbolo de multiplicación.
	call	LCD_Caracter
	movlw	HIGH MULTIPLICADOR		; Repite lo mismo con el multiplicador.
	movwf	Arit_Multiplicador_H
	call	LCD_ByteCompleto
	movlw	LOW MULTIPLICADOR
	movwf	Arit_Multiplicador_L
	call	LCD_ByteCompleto
	movlw	'='						; Visualiza signo de igualdad.
	call	LCD_Caracter
	call	LCD_Linea2
	call	Arit_Multiplica_16Bit	; Realiza la multiplicación.
	movf	Arit_Producto_2H,W		; Y ahora visualiza el resultado en 32 bits.
	call	LCD_ByteCompleto
	movf	Arit_Producto_2L,W
	call	LCD_ByteCompleto
	movf	Arit_Producto_1H,W
	call	LCD_ByteCompleto
	movf	Arit_Producto_1L,W
	call	LCD_ByteCompleto
	sleep							; Pasa a modo de bajo consumo.

	INCLUDE	<ARITMETICA.INC>		; Subrutina Arit_Multiplica_16Bit
	INCLUDE	<RETARDOS.INC>
	INCLUDE	<LCD_4BIT.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. López.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================

