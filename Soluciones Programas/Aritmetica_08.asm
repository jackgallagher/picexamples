;********************************* Aritmetica_08.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Programa donde se comprueba la forma de dividir dos n�meros de 8 bits con un ejemplo
; visualizado en el LCD. La operaci�n se realiza sin signo.
;
; Utiliza la subrutina Arit_Divide_8Bit de la libreria Aritmetica.inc que realiza la
; divisi�n como indica:
; (Arit_Dividendo) / (Arit_Divisor) = (Arit_Cociente)
; y el resto en (Arit_Resto)
;
; ZONA DE DATOS *************************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	ENDC

DIVIDENDO	EQU	0xFB				; Por ejemplo utilizar� estos n�meros.
DIVISOR		EQU	0x0A

; ZONA DE C�DIGOS ***********************************************************************

	ORG 	0
Inicio
	call	LCD_Inicializa
	movlw	DIVISOR					; Carga el divisor y comprueba que no es igual a cero.
	movwf	Arit_Divisor
	movf	Arit_Divisor,F
	btfss	STATUS,Z
	goto	Divide	
	movlw	MensajeError			; Si el divisor es cero, visualiza un mensaje de error.
	call	LCD_Mensaje
	goto	Fin
Divide	
	movlw	DIVIDENDO				; Extrae los bytes, los lleva a los registros
	movwf	Arit_Dividendo			; utilizados por Arit_Divide_8Bit.
	call	LCD_ByteCompleto		; Visualiza el dividendo.
	movlw	0xFD					; Posici�n del signo de la divisi�n para LCD,
	call	LCD_Caracter			; (ver tabla de la figura 13-4 del libro).
	movf	Arit_Divisor,W
	call	LCD_ByteCompleto
	movlw	'='						; Visualiza signo de igualdad.
	call	LCD_Caracter
	call	Arit_Divide_8Bit		; Realiza la divisi�n.
	movf	Arit_Cociente,W			; Y ahora visualiza el cociente.
	call	LCD_ByteCompleto
	call	LCD_Linea2				; Ahora visualiza el resto en la segunda l�nea.
	movlw	MensajeResto
	call	LCD_Mensaje
	movf	Arit_Resto,W
	call	LCD_ByteCompleto
Fin	sleep							; Pasa a modo de bajo consumo.

Mensajes
	addwf	PCL,F
MensajeError
	DT "Divide por CERO!",0x00
MensajeResto
	DT "Resto=", 0x00

	INCLUDE	<ARITMETICA.INC>		; Subrutina Arit_Divide_8Bit.
	INCLUDE	<RETARDOS.INC>
	INCLUDE	<LCD_4BIT.INC>
	INCLUDE  <LCD_MENS.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================

