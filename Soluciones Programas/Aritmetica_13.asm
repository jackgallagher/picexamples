;********************************* Aritmetica_13.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. López.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Programa ejemplo de utilización de subrutinas aritméticas. En el LCD se visualizará una
; multiplicación decimal, por ejemplo "109*68=7412"
;
; Utiliza la subrutina Arit_Multiplica_8Bit de la libreria Aritmetica.inc que realiza la
; multiplicación en 8 bits y el resultado lo guarda en 16 bits utilizando los siguientes
; registros:
; (Arit_Multiplicando) * (Arit_Multiplicador) = (Arit_Producto_H, Arit_Producto_L).
;
; La multiplicación se realiza sin signo.
;
; ZONA DE DATOS *************************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	ENDC

MULTIPLICANDO	EQU	.109			; Por ejemplo utilizará estos números.
MULTIPLICADOR	EQU	.68

; ZONA DE CÓDIGOS ***********************************************************************

	ORG 	0
Inicio
	call	LCD_Inicializa
	clrf	Arit_Binario_H
	movlw	MULTIPLICANDO			; Visualiza el multiplicando en decimal.
	movwf	Arit_Binario_L
	call	Arit_Bin_BCD_16Bit		; Pasa de binario natural en 16 bits a BCD de 5 digitos.
	call	Arit_VisualizaBCD		; Lo visualiza.
	movlw	'*'						; Visualiza símbolo de la multiplicación.
	call	LCD_Caracter			; (ver tabla de la figura 13-4 del libro).
	clrf	Arit_Binario_H			; Visualiza el multiplicador en decimal.
	movlw	MULTIPLICADOR
	movwf	Arit_Binario_L
	call	Arit_Bin_BCD_16Bit		; Pasa de binario natural en 16 bits a BCD de 5 digitos.
	call	Arit_VisualizaBCD		; Lo visualiza
	movlw	'='						; Visualiza signo de igualdad.
	call	LCD_Caracter
;
	movlw	MULTIPLICANDO			; Carga todos los registros para realizar la multiplicación.
	movwf	Arit_Multiplicando
	movlw	MULTIPLICADOR
	movwf	Arit_Multiplicador
	call	Arit_Multiplica_8Bit	; Realiza la multiplicación.
	movf	Arit_Producto_H,W		; Y ahora visualiza el resultado.
	movwf	Arit_Binario_H
	movf	Arit_Producto_L,W
	movwf	Arit_Binario_L
	call	Arit_Bin_BCD_16Bit		; Pasa de binario natural en 16 bits a BCD de 5 digitos.
	call	Arit_VisualizaBCD		; Visualiza el resultado.
	sleep							; Pasa a modo de bajo consumo.

	INCLUDE	<ARITMETICA.INC>
	INCLUDE	<RETARDOS.INC>
	INCLUDE	<LCD_4BIT.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. López.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
