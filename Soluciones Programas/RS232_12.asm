;************************************ RS232_12.asm **************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Programa que visualiza en el m�dulo LCD el c�digo ASCII en formato decimal de la
; tecla pulsada y tambi�n en el monitor del ordenador en formato hexadecimal. Por ejemplo:
;    - 	Monitor del ordenador: " k-6B", donde la "k" es la tecla pulsada y el "6B" el
;		c�digo ASCII en formato hexadecimal."
;    - 	Pantalla del LCD: " k-107", donde la "k" es la tecla pulsada y el "107" el
;		c�digo ASCII en formato decimal.
;
; ZONA DE DATOS **********************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	GuardaDato
	ENDC

; ZONA DE C�DIGOS ********************************************************************

	ORG	0
Inicio
	call	LCD_Inicializa
	call	RS232_Inicializa
Principal
	call	RS232_LeeDato				; Espera recibir un car�cter.
	movwf	GuardaDato					; Guarda el dato recibido.
	call	LCD_Borra					; Despeja la pantalla del visualizador LCD.
	movf	GuardaDato,W				; Recupera el dato le�do.
	call	LCD_Caracter				; Visualiza el car�cter de la tecla pulsada.
	movlw	'-'							; Visualiza el gui�n.
	call	LCD_Caracter
	movf	GuardaDato,W				; Lo pasa a BCD antes de visualizarlo.
	call	BIN_a_BCD
	movf	BCD_Centenas,W				; Visualiza el resultado en la pantalla  del
	call	LCD_Nibble					; LCD comenzando por las centenas.
	movf	BCD_Decenas,W
	call	LCD_Nibble
	movf	BCD_Unidades,W
	call	LCD_Nibble
	movf	GuardaDato,W				; Visualiza en la pantalla del ordenador el 
	call	RS232_EnviaDato				; car�cter pulsado.
	movlw	'-'							; Visualiza en el ordenador el gui�n.
	call	RS232_EnviaDato
	swapf	GuardaDato,W				; Ahora visualiza en formato hexadecimal
	call	RS232_Nibble				; el nibble alto.
	movf	GuardaDato,W				; Visualiza en formato hexadecimal el nibble
	call	RS232_Nibble				
	movlw	' '							; Env�a un espacio en blanco para separarlo del
	call	RS232_EnviaDato				; anterior.
	movlw	' '
	call	RS232_EnviaDato
	goto	Principal
;
; Subrutina "RS232_Nibble" --------------------------------------------------------------
; 
; Visualiza en el lugar actual de la pantalla, el valor hexadecimal que almacena en el
; nibble bajo del registro W. El nibble alto de W no es tenido en cuenta. Ejemplos:
; - Si (W)=b'01010110', se visualizar� "6". 
; - Si (W)=b'10101110', se visualizar� "E". 

	CBLOCK
	RS232_GuardaNibble
	ENDC

RS232_Nibble
	andlw	b'00001111'					; Se queda con la parte baja.
	movwf	RS232_GuardaNibble			; Lo guarda.
	sublw	0x09						; Comprueba si hay que representarlo con letra.
	btfss	STATUS,C	
	goto	RS232_EnviaByteLetra
	movf	RS232_GuardaNibble,W
	addlw	'0'							; El n�mero se pasa a car�cter ASCII sum�ndole
	goto 	RS232_FinVisualizaDigito	; el ASCII del cero y lo visualiza.
RS232_EnviaByteLetra
	movf	RS232_GuardaNibble,W
	addlw	'A'-0x0A					; S�, por tanto, se le suma el ASCII de la 'A'.
RS232_FinVisualizaDigito
	call	RS232_EnviaDato				; Y visualiza el car�cter.
	return
;
	INCLUDE  <RS232.INC>
	INCLUDE  <LCD_4BIT.INC>
	INCLUDE  <RETARDOS.INC>
	INCLUDE  <BIN_BCD.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
