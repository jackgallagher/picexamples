;********************************* Aritmetica_10.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Programa donde se comprueba la forma de convertir un n�mero BCD de 5 digitos a un n�mero
; binario natural de 16 bits con un ejemplo visualizado en el LCD.
; El m�ximo n�mero de entrada ser� pues el 65535.
;
; Utiliza la subrutina Arit_BCD_Bin_16Bit de la librer�a Arit_Aritmetica.inc, donde los
; digitos de entrada se introducen por los 5 registros:
; (Arit_DecenasMillar, Arit_Millares, Arit_Centenas, Arit_Decenas y Arit_Unidades) y 
; el resultado se obtiene en 16 bits, es decir en 2 registros de 8 bits, que son:
; (Arit_Resultado_1H, Arit_Resultado_1L)
;
; ZONA DE DATOS *************************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	ENDC

UNIDADES		EQU	.2				; El n�mero a convertir ser� por ejemplo el 56362.
DECENAS			EQU	.6
CENTENAS		EQU	.3
MILLARES		EQU	.6
DECENAS_MILLAR	EQU	.5

; ZONA DE C�DIGOS ***********************************************************************

	ORG 	0
Inicio
	call	LCD_Inicializa
	movlw	MensajeBCD
	call	LCD_Mensaje
	movlw	DECENAS_MILLAR			; Carga todos los registros y visualiza los digitos.
	movwf	Arit_DecenasMillar
	call	LCD_Nibble
	movlw	MILLARES
	movwf	Arit_Millares
	call	LCD_Nibble
	movlw	CENTENAS
	movwf	Arit_Centenas
	call	LCD_Nibble
	movlw	DECENAS
	movwf	Arit_Decenas
	call	LCD_Nibble
	movlw	UNIDADES
	movwf	Arit_Unidades
	call	LCD_Nibble
	call	Arit_BCD_Bin_16Bit		; Pasa de BCD a Binario natural en 16 bits.
	call	LCD_Linea2				; Pasa a la segunda l�nea del LCD.
	movlw	MensajeHex				; Visualiza el resultado en hexadecimal.
	call	LCD_Mensaje
	movf	Arit_Resultado_1H,W
	call	LCD_ByteCompleto
	movf	Arit_Resultado_1L,W
	call	LCD_ByteCompleto
	sleep							; Pasa a modo de bajo consumo.

Mensajes
	addwf	PCL,F
MensajeBCD
	DT "Dec. = ",0x00
MensajeHex
	DT "Hex. = ", 0x00

	INCLUDE	<ARITMETICA.INC>		; Subrutina Arit_BCD_Bin_16Bit.
	INCLUDE	<RETARDOS.INC>
	INCLUDE	<LCD_4BIT.INC>
	INCLUDE <LCD_MENS.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
