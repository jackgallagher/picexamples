;********************************* Aritm�tica_02.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; En el visualizador LCD aparece en binario el valor de una constante de 16 bits.
; Utiliza la subrutina Arit_VisualizaBinario de la liber�a Aritmetica.inc
;
; ZONA DE DATOS *************************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	ENDC

NUMERO_A	EQU	0x1B3C					; Por ejemplo este n�mero de dos bytes (16 bits).

; ZONA DE C�DIGOS ***********************************************************************

	ORG 	0
Inicio
	call	LCD_Inicializa
	movlw	HIGH NUMERO_A			; El operador HIGH extrae el byte alto.
	call	LCD_ByteCompleto		; Visualiza en hexadecimal.
	movlw	LOW NUMERO_A			; El operador LOW extrae el byte bajo.
	call	LCD_ByteCompleto		; Visualiza en hexadecimal.
	call	LCD_Linea2
	movlw	HIGH NUMERO_A			; El operado HIGH extrae el byte alto.
	call	Arit_VisualizaBinario	; Visualiza en binario.
	movlw	LOW NUMERO_A			; El operado LOW extrae el byte bajo.
	call	Arit_VisualizaBinario	; Visualiza en binario.
	sleep							; Pasa a reposo.

	INCLUDE	<RETARDOS.INC>
	INCLUDE	<LCD_4BIT.INC>
	INCLUDE	<ARITMETICA.INC>		; Subrutina Arit_VisualizaBinario.
	END

;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
