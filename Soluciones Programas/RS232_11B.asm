;************************************ RS232_11B.asm **************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Ejercicio RS232_11.asm resuelto de una forma m�s eficaz.
;
; ZONA DE DATOS **********************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK   0x0C		
	TeclaPulsada					; Va a guardar el contenido de la tecla pulsada.
	MensajeApuntado					; Va a guarda la direcci�n del mensaje apuntado.
	ENDC

#DEFINE  SalidaAdelante		PORTB,3	; Define d�nde se sit�an las salidas.
#DEFINE  SalidaAtras		PORTB,2
#DEFINE  SalidaIzquierda	PORTB,1
#DEFINE  SalidaDerecha		PORTB,0

TECLA_ADELANTE	EQU	't'				; C�digo de las teclas utilizadas.
TECLA_ATRAS		EQU	'b'
TECLA_IZQ		EQU	'a'
TECLA_DER		EQU	'l'
TECLA_PARADA	EQU	' '				; C�digo de la tecla espaciadora, (hay un espacio,
									; tened cuidado al teclear el programa).
; ZONA DE C�DIGOS ********************************************************************

	ORG	0
Inicio
	call	LCD_Inicializa
	call	RS232_Inicializa
	bsf		STATUS,RP0				; Configura como salidas las 4 l�neas del
	bcf		SalidaAdelante			; del Puerto B respetando la configuraci�n del
	bcf		SalidaAtras				; resto de las l�neas.
	bcf		SalidaIzquierda
	bcf		SalidaDerecha
	bcf		STATUS,RP0
	call	Parado					; En principio todas las salidas deben estar 
Principal							; apagadas.
	call	RS232_LeeDato			; Espera a recibir un car�cter.
	clrf	PORTB
	call	TesteaTeclado
	goto	Principal

; "Mensajes" ----------------------------------------------------------------------------
;
Mensajes
	addwf	PCL,F
MensajeParado
	DT "Sistema PARADO", 0x00
MensajeAdelante
	DT "Marcha ADELANTE", 0x00
MensajeAtras
	DT "Marcha ATRAS", 0x00
MensajeIzquierda
	DT "Hacia IZQUIERDA", 0x00
MensajeDerecha
	DT "Hacia DERECHA", 0x00

; Subrutina "TesteaTeclado" -------------------------------------------------------------
;
; Testea el teclado y act�a en consecuencia.

TesteaTeclado
	bcf		SalidaAdelante			; Primero apaga todas las salidas.
	bcf		SalidaAtras	
	bcf		SalidaIzquierda
	bcf		SalidaDerecha
;
	movwf	TeclaPulsada			; Guarda el contenido de la tecla pulsada.
	xorlw	TECLA_ADELANTE			; �Es la tecla del movimiento hacia adelante?
	btfsc	STATUS,Z
	goto	Adelante				; S�, se desea movimiento hacia adelante.
;
	movf	TeclaPulsada,W			; Recupera el contenido de la tecla pulsada.
	xorlw	TECLA_ATRAS				; �Es la tecla del movimiento hacia atr�s?
	btfsc	STATUS,Z
	goto	Atras					; S�, se desea movimiento hacia atr�s.
;
	movf	TeclaPulsada,W			; Recupera el contenido de la tecla pulsada.
	xorlw	TECLA_IZQ				; �Es la tecla del movimiento hacia la izquierda?
	btfsc	STATUS,Z
	goto	Izquierda				; S�, se desea movimiento hacia la izquierda.
;
	movf	TeclaPulsada,W			; Recupera el contenido de la tecla pulsada.
	xorlw	TECLA_DER				; �Es tecla del movimiento hacia la derecha?
	btfsc	STATUS,Z
	goto	Derecha					; S�, se desea movimiento hacia la derecha.
;
	movf	TeclaPulsada,W			; Recupera el contenido de la tecla pulsada.
	xorlw	TECLA_PARADA			; �Es la tecla de parada?.
	btfss	STATUS,Z
	goto	Fin						; No es ninguna tecla de movimiento. Sale.
Parado
	movlw	MensajeParado
	goto	Visualiza
Adelante
	bsf		SalidaAdelante
	movlw	MensajeAdelante
	goto	Visualiza
Atras
	bsf		SalidaAtras
	movlw	MensajeAtras
	goto	Visualiza
Izquierda
	bsf		SalidaIzquierda
	movlw	MensajeIzquierda
	goto	Visualiza
Derecha
	bsf		SalidaDerecha
	movlw	MensajeDerecha

; Seg�n el estado de las salidas visualiza el estado del sistema en el visualizador LCD y en
; el monitor del ordenador.

Visualiza
	movwf	MensajeApuntado			; Guarda la posici�n del mensaje.
	call	LCD_Borra				; Borra la pantalla del modulo LCD.
	movf	MensajeApuntado,W		; Visualiza el mensaje en la pantalla
	call	LCD_Mensaje				; del visualizador LCD.
	call	RS232_LineasBlanco		; Borra la pantalla del ordenador.
	movf	MensajeApuntado,W
	call	RS232_Mensaje			; Lo visualiza en el HyperTerminal.
	call	RS232_LineasBlanco
Fin	return

	INCLUDE  <RS232.INC>
	INCLUDE  <RS232MEN.INC>
	INCLUDE  <LCD_4BIT.INC>
	INCLUDE  <LCD_MENS.INC>
	INCLUDE  <RETARDOS.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
