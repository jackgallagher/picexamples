;********************************* Aritmetica_09.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Programa donde se comprueba la forma de dividir un n�mero de 16 bits entre otro
; de 7 bits mediante un ejemplo visualizado en el LCD.
;
; Utiliza la subrutina Arit_Divide_16Bit de la libreria Aritmetica.inc que realiza la
; divisi�n como se indica mediante los siguientes registros:
; (Arit_Dividendo_H, Arit_Dividendo_L) / (Arit_Divisor) = (Arit_Cociente_H, Arit_Cociente_L)
; y el resto en (Arit_Resto).
;
; La divisi�n se har� sin signo y el divisor ser� de 7 bits, es decir su valor m�ximo ser�:
; b'01111111'=0x7F=d'127'.
;
; ZONA DE DATOS *************************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	ENDC

DIVIDENDO	EQU	0xA1DA				; Por ejemplo utilizar� estos n�meros.
DIVISOR		EQU	0x6D

; ZONA DE C�DIGOS ***********************************************************************

	ORG 	0
Inicio
	call	LCD_Inicializa
	movlw	DIVISOR					; Carga el divisor y comprueba que no es igual a cero.
	movwf	Arit_Divisor
	movf	Arit_Divisor,F
	btfss	STATUS,Z
	goto	Divide	
	movlw	MensajeError			; Si el divisor es cero, visualiza un mensaje de error.
	call	LCD_Mensaje
	goto	Fin
Divide
	movlw	HIGH DIVIDENDO			; Extrae los bytes y los lleva a los registros del 
	movwf	Arit_Dividendo_H		; dividendo utilizados por Arit_Divide_16Bit.
	call	LCD_ByteCompleto		; Visualiza el dividendo.
	movlw	LOW DIVIDENDO
	movwf	Arit_Dividendo_L
	call	LCD_ByteCompleto
	movlw	0xFD					; Posici�n del signo de la divisi�n para LCD,
	call	LCD_Caracter			; (ver tabla de la figura 13-4 del libro).
	movf	Arit_Divisor,W
	call	LCD_ByteCompleto
	movlw	'='						; Visualiza signo de igualdad.
	call	LCD_Caracter
	call	Arit_Divide_16Bit		; Realiza la divisi�n.
	movf	Arit_Cociente_H,W		; Y ahora visualiza el cociente.
	call	LCD_ByteCompleto
	movf	Arit_Cociente_L,W
	call	LCD_ByteCompleto
	call	LCD_Linea2				; Ahora visualiza el resto en la segunda l�nea.
	movlw	MensajeResto
	call	LCD_Mensaje
	movf	Arit_Resto,W
	call	LCD_ByteCompleto
Fin	sleep							; Pasa a modo de bajo consumo.

Mensajes
	addwf	PCL,F
MensajeError
	DT "Divide por CERO!",0x00
MensajeResto
	DT "Resto = ", 0x00

	INCLUDE	<ARITMETICA.INC>		; Subrutina Arit_Divide_16Bit.
	INCLUDE	<RETARDOS.INC>
	INCLUDE	<LCD_4BIT.INC>
	INCLUDE <LCD_MENS.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
