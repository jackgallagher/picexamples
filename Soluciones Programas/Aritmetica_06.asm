;********************************* Aritmetica_06.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. López.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Programa donde se comprueba la forma de multiplicar dos números de 8 bits con un ejemplo
; visualizado en el LCD.
;
; Utiliza la subrutina Arit_Multiplica_8Bit de la libreria Aritmetica.inc que realiza la
; multiplicación en 8 bits y el resultado lo guarda en 16 bits utilizando los siguientes
; registros:
; (Arit_Multiplicando) * (Arit_Multiplicador) = (Arit_Producto_H, Arit_Producto_L).
;
; La multiplicación se realiza sin signo.
;
; ZONA DE DATOS *************************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	ENDC

MULTIPLICANDO	EQU	0xF6			; Por ejemplo realizará la multiplicación de estos
MULTIPLICADOR	EQU	0xA8			; número de 8 bits: Multiplicando * Multiplicador.

; ZONA DE CÓDIGOS ***********************************************************************

	ORG 	0
Inicio
	call	LCD_Inicializa
	movlw	MULTIPLICANDO			; Pasa el multiplicando al registro correspondiente.
	movwf	Arit_Multiplicando		; utilizados por la subrutina Arit_Multiplica_8bit.
	call	LCD_ByteCompleto		; Lo visualiza.
	movlw	'*'						; Visualiza signo de la multiplicación.
	call	LCD_Caracter
	movlw	MULTIPLICADOR			; Pasa el multiplicador al registro correspondiente.
	movwf	Arit_Multiplicador
	call	LCD_ByteCompleto		; Visualiza el multiplicador.
	movlw	'='						; Visualiza signo de igualdad.
	call	LCD_Caracter
	call	Arit_Multiplica_8Bit	; Realiza la multiplicación.
	movf	Arit_Producto_H,W		; y ahora visualiza el resultado.
	call	LCD_ByteCompleto
	movf	Arit_Producto_L,W
	call	LCD_ByteCompleto
	sleep

	INCLUDE	<ARITMETICA.INC>		; Subrutina Arit_Multiplica_8Bit
	INCLUDE	<RETARDOS.INC>
	INCLUDE	<LCD_4BIT.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. López.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
