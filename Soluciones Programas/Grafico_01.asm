;************************************** Grafico_01.asm **********************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Muestra un dibujo con el contorno de una pila.
;
; ZONA DE DATOS *************************************************************************

	LIST	 P=16F84A
	INCLUDE  <P16F84A.INC>
	__CONFIG   _CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC
	
	CBLOCK 0x0C
	Contador
	ENDC

; ZONA DE C�DIGOS ********************************************************************

	ORG	0
Inicio
	call	LCD_Inicializa			; Prepara la pantalla LCD.
	call	CGRAM_EscribeDatos
Principal
	call	LCD_Borra				; Limp�a la pantalla LCD.
	clrw							; El s�mbolo est� en la posici�n 0 de la CGRAM.
	call	LCD_Caracter			; Lo visualiza.
	sleep							; Pasa a bajo consumo
	goto	Principal
	
; Subrutina LCD_Escribe CGRAM -----------------------------------------------------------
;
; Escribe en la CGRAM el s�mbolo de la pila descargada.
;
CGRAM_NUM_IMAGENES EQU 1			; Se va a grabar un solo s�mbolo
;
CGRAM_EscribeDatos
	movlw 	b'01000000'				; Comando indicando que va a escribir a partir de
	call 	LCD_EnviaComando		; la direcci�n 0 de la CGRAM.
	clrf	Contador
CGRAM_EscribeOtro
	movf	Contador,W
	call	CGRAM_Escribe
	call	LCD_Caracter
	incf	Contador,F
	movlw	CGRAM_NUM_IMAGENES * 8	; Cada imagen ocupa 8 posiciones de CGRAM.
	subwf	Contador,W
	btfss	STATUS,C
	goto	CGRAM_EscribeOtro	
	call	LCD_Borra
	return	

CGRAM_Escribe
	addwf	PCL,F
	
; Bateria al 0%, byte 0 de la CGRAM.
							;   ________
	retlw	b'00001110'		;  |    xxx |
	retlw	b'00001010'		;  |    x x |
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00010001'		;  |   x   x|
	retlw	b'00011111'		;  |   xxxxx|
							;   --------
	INCLUDE  <RETARDOS.INC>
	INCLUDE  <LCD_4BIT.INC>

	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================

