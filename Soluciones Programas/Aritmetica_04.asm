;********************************* Aritmetica_04.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Programa donde se comprueba la forma de sumar dos n�meros de 16 bit, mediante un ejemplo
; que se visualiza en un LCD.
;
; Utiliza la subrutina Arit_Suma_16Bit de la librer�a Aritmetica.inc que realiza la suma
; en 16 bits: (Operando 2) + (Operando 1) y el resultado lo guarda en (Operando 2) en 16 bits.
; Es decir: (Arit_Operando_2H, Arit_Operando_2L) + (Arit_Operando_1H, Arit_Operando_1L) -->
; (Arit_Operando_2H, Arit_Operando_2L)
;
; ZONA DE DATOS *************************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	ENDC

SUMANDO_A	EQU	0x70F6				; Por ejemplo realizar� la suma de estos n�meros de
SUMANDO_B	EQU	0xF8AB				; dos bytes 16 bits): SUMANDO_A + SUMANDO_B

; ZONA DE C�DIGOS ***********************************************************************

	ORG 	0
Inicio
	call	LCD_Inicializa
	movlw	HIGH SUMANDO_A			; Extrae los bytes y los lleva a los registros
	movwf	Arit_Operando_2H		; utilizados por la subrutina Arit_Suma_16bit.
	call	LCD_ByteCompleto
	movlw	LOW SUMANDO_A
	movwf	Arit_Operando_2L
	call	LCD_ByteCompleto
	movlw	'+'						; Visualiza signo de suma.
	call	LCD_Caracter
	movlw	HIGH SUMANDO_B
	movwf	Arit_Operando_1H
	call	LCD_ByteCompleto
	movlw	LOW SUMANDO_B
	movwf	Arit_Operando_1L
	call	LCD_ByteCompleto
	movlw	'='						; Visualiza signo de igualdad.
	call	LCD_Caracter
	call	Arit_Suma_16Bit			; Realiza la suma.
	movlw	0x01					; Supone que no hay acarreo
	btfsc	STATUS,C				; Debe visualizar el acarreo en caso que lo haya.
	call	LCD_ByteCompleto	
	movf	Arit_Operando_2H,W		; Y ahora visualiza el resultado.
	call	LCD_ByteCompleto
	movf	Arit_Operando_2L,W
	call	LCD_ByteCompleto
	sleep							; Pasa a modo bajo consumo.

	INCLUDE	<ARITMETICA.INC>		; Subrutina Arit_Suma_16Bit.
	INCLUDE	<RETARDOS.INC>
	INCLUDE	<LCD_4BIT.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================

