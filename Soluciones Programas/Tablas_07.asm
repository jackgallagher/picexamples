;************************************* Tablas_07.asm *************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Repetir el ejercicio Tablas_03.asm, pero situando la tabla en una posici�n tal que desborde
; la p�gina de las primeras 256 posiciones de memoria de programa ROM.
;
; ZONA DE DATOS **********************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC
	
	CBLOCK 0x0C
	GuardaOffset
	ENDC

; ZONA DE C�DIGOS ********************************************************************

	ORG 	0					; El programa comienza en la direcci�n 0.
Inicio
	bsf		STATUS,RP0			; Acceso al Banco 1.
	clrf	TRISB				; Las l�neas del Puerto B se configuran como salida.
	movlw	b'00011111'			; Las 5 l�neas del Puerto A se configuran como entrada.
	movwf	TRISA
	bcf		STATUS,RP0			; Acceso al Banco 0.
Principal
	movf	PORTA,W				; Lee el valor de las variables de entrada.
	andlw	b'00000111'			; Se queda con los tres bits bajos de entrada.
	movwf	GuardaOffset		; Guarda el offset del salto.	
	movlw	LOW (TablaVerdad+1)	; Obtiene los 8 bits bajos de la direcci�n de memoria
								; de programa donde est� situada la tabla real.
	addwf	GuardaOffset,F		; Y halla el valor del salto absoluto dentro de la tabla.
	movlw	HIGH (TablaVerdad+1); Cinco bits alto de la direcci�n de memoria de
								; programa donde est� situada la tabla real.
	btfsc	STATUS,C			; �Ha desbordado la p�gina de 256 bytes?
	addlw	1					; S�, entonces a�ade una unidad al PCH.
	movwf	PCLATH				; Prepara el PCLATH.
	movf	GuardaOffset,W		; El offset se cargar� en el PCL.
	call	TablaVerdad 		; Obtiene la configuraci�n de salida.
	movwf	PORTB				; Se visualiza por el puerto de salida.
	goto 	Principal

; Subrutina "TablaVerdad" ---------------------------------------------------------------
;
; La tabla de la verdad se sit�a desbordando los primeros 256 bytes de memoria de programa.

	ORG	.254					; Cerca del borde de la primera p�gina de 256 bytes.

TablaVerdad						; Posici�n 0x00FE de memoria de programa (PCH-PCL).
	movwf	PCL					; El salto dentro de la tabla: PCH=PCLATH, PCL=W.
								; (Ver figura 9-8).
	retlw 	b'00001010'			; (Configuraci�n 0). Posici�n 0x00FF.
	retlw 	b'00001001'			; (Configuraci�n 1). Posici�n 0x0100.
	retlw 	b'00100011'			; (Configuraci�n 2). Posici�n 0x0101.
	retlw 	b'00001111'			; (Configuraci�n 3). Posici�n 0x0102.
	retlw 	b'00100000'			; (Configuraci�n 4). Posici�n 0x0103.
	retlw 	b'00000111'			; (Configuraci�n 5). Posici�n 0x0104.
	retlw 	b'00010111'			; (Configuraci�n 6). Posici�n 0x0105.
	retlw 	b'00111111'			; (Configuraci�n 7). Posici�n 0x0106.

	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
