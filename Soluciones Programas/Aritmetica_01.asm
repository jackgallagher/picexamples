;********************************* Aritm�tica_01.asm ************************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Programa para comprobar el funcionamiento de los operandos UPPER, HIGH y LOW. Para ello:
; - En la primera l�nea del LCD se visualiza un n�mero de dos bytes.
; - En la segunda l�nea del LCD se visualiza un n�mero de tres bytes.
;
; ZONA DE DATOS *************************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC

	CBLOCK	0x0C
	ENDC

NUMERO_A	EQU	0x1B3C					; Por ejemplo este n�mero de dos bytes.
NUMERO_B	EQU	0xF85ACB				; Por ejemplo este n�mero de tres bytes.

; ZONA DE C�DIGOS ***********************************************************************

	ORG 	0
Inicio
	call	LCD_Inicializa
	call	LCD_Linea1				; Coloca el cursor al principio de la pantalla.
	movlw	HIGH NUMERO_A			; El operador HIGH extrae el byte alto de 
	call	LCD_Byte				; de dos.
	movlw	LOW NUMERO_A			; El operador LOW extrae el byte m�s bajo de 
	call	LCD_ByteCompleto		; dos.
	call	LCD_Linea2				; Coloca el cursor al principio de la segunda l�nea.
	movlw	UPPER NUMERO_B			; El operador UPPER extrae el byte m�s alto de 
	call	LCD_Byte				; tres.
	movlw	HIGH NUMERO_B			; El operador HIGH extrae el byte medio de 
	call	LCD_ByteCompleto		; tres.
	movlw	LOW NUMERO_B			; El operador LOW extrae el byte m�s bajo de 
	call	LCD_ByteCompleto		; tres.
	sleep							; Pasa a modo de bajo consumo.

	INCLUDE	<RETARDOS.INC>
	INCLUDE	<LCD_4BIT.INC>
	END
	
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
