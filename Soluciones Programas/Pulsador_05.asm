;************************************ Pulsador_05.asm ***********************************
;
;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
;
; Cada vez que presiona el pulsador conectado a la l�nea RA4 conmutar� el estado de
; un LED conectado a la l�nea RB1.
;
; ZONA DE DATOS **********************************************************************

	LIST		P=16F84A
	INCLUDE		<P16F84A.INC>
	__CONFIG	_CP_OFF &  _WDT_OFF & _PWRTE_ON & _XT_OSC
	
	CBLOCK 0x0C
	ENDC

#DEFINE Pulsador	PORTA,4		; Pulsador conectado a RA4.
#DEFINE LED			PORTB,1		; L�nea donde se conecta el diodo LED.

; ZONA DE C�DIGOS ********************************************************************

	ORG	0						; El programa comienza en la direcci�n 0.
Inicio
 	bsf		STATUS,RP0			; Acceso al Banco 1.
	bsf		Pulsador			; La l�nea RA4 se configura como entrada.
	bcf		LED					; Se configura como salida.
	bcf		STATUS,RP0			; Acceso al Banco 0.
	bcf		LED					; En principio diodo LED apagado.
Principal
	btfsc	Pulsador			; �Pulsador presionado?, �(Pulsador)=0?
	goto	Fin					; No. Vuelve a leerlo.
	call	Retardo_20ms		; Espera que se estabilicen los niveles de tensi�n.
	btfsc	Pulsador			; Comprueba si es un rebote.
	goto	Fin					; Era un rebote y sale fuera.
	btfsc	LED					; Testea el �ltimo estado del LED.
 	goto	EstabaEncendido
EstabaApagado
	bsf		LED					; Estaba apagado y lo enciende.
	goto	EsperaDejePulsar
EstabaEncendido
	bcf		LED					; Estaba encendido y lo apaga.
EsperaDejePulsar
	btfss	Pulsador			; �Dej� de pulsar?. �(Pulsador)=1?
	goto	EsperaDejePulsar	; No. Espera que deje de pulsar.
Fin
	goto	Principal

	INCLUDE <RETARDOS.INC>
	END

;	===================================================================
;	  Del libro "MICROCONTROLADOR PIC16F84. DESARROLLO DE PROYECTOS"
;	  E. Palacios, F. Remiro y L. L�pez.		www.pic16f84a.com
; 	  Editorial Ra-Ma.  www.ra-ma.es
;	===================================================================
